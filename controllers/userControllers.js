const User = require("../models/User")
const bcrypt = require("bcrypt")

const auth = require("../auth")



module.exports.registerUser = (reqBody) => {

	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {

		if(error){
			return false
		}else{
			return true
		}
	})
}


module.exports.loginUser = (reqBody) => {
	return User.findOne({ email: reqBody.email}).then(result => {
		if(result == null){
			return false
		}else{
		
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){
				return { accessForToken: auth.createAccessToken(result.toObject()) }
			}else{
				return false
			}
		}
	})
}

module.exports.getAllCourses = () => {
	return User.find({}, {password:0}).then(result => {
		return result
	})
}


module.exports.getSpecificData	 = (reqBody) => {
	return User.findOne({email: reqBody.email}, {password: 0}).then(result => {
		return result
	})
}




