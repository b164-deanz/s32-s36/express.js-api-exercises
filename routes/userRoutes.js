const express = require("express")
const router = express.Router()

const UserController = require("../controllers/userControllers")


const auth = require("../auth");




router.post("/registerUser", (req, res) => {
	UserController.registerUser(req.body).then(result => res.send(result))
})


router.post("/loginUser", (req, res) => {
	UserController.loginUser(req.body).then(result => res.send(result))
})


router.get("/all" , (req, res) => {
	UserController.getData().then(result => res.send(result))
})

router.get("/", (req, res) =>{
	UserController.getSpecificData(req.body).then(result => res.send(result))
})



module.exports = router;